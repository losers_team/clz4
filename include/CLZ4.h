#include <fstream>
#include <lz4.h>
#include <string>
#ifndef CLZ4_H
#define CLZ4_H
namespace X {
/**
 * @brief The CLZ4 class is a very basic wrapper to compress and decompress file
 * using lz4 algorithm. Becouse of simplicity, class don't have constructor but
 * only few static methods.
 */
class CLZ4 {
public:
  /**
   * @brief This function try to compress file, using lz4 algorithm. As a
   * argument, function take path to file which should be compressed, and path
   * to expected result. When compression was ended with sucess, function return
   * true. If somthing went wrong, false will be returned.
   * @param inPath - path file to compress
   * @param outPath - compressed file path
   * @return - true if file was compressed correct in other hance false
   */
  static bool compressFile(std::string inPath, std::string outPath);
  /**
   * @brief This function try to decompress file, compressed by lz4 algorithm.
   * As a argument, function take path to file which should be decompressed, and
   * path to expected result. When decompression was ended with sucess, function
   * return true. If somthing went wrong function return false.
   * @param inPath -  file path to decompress
   * @param outPath - path to decompressed
   * @return - true if file was decompressed correct in other hance false
   */
  static bool decompressFile(std::string inPath, std::string outPath);
};
} // namespace X
#endif
