#include "CLZ4.h"
#include <iostream>
#include <ctime>
#include <regex>
#include <lz4.h>
bool X::CLZ4::compressFile( std::string inPath, std::string outPath)
{
	bool result = false;
	std::ifstream file;
	file.open(inPath, std::ios_base::in);
	if( file.is_open())
	{
		file.seekg (0, std::ifstream::end);
		int size = file.tellg();
		file.clear ();
		file.seekg ( 0, std::ifstream::beg );
		char* buffer = new char[size];
		file.read( buffer, size );
		file.close ();
		char* compressedData ;
		const int maxCompresedSize = LZ4_compressBound(size)+1;
		try
		{
			compressedData = new char[maxCompresedSize];
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			delete [] buffer;
			return result;
		}
		int compressedDataSize = LZ4_compress_default ( (const char*) (buffer), compressedData, size, maxCompresedSize);
		if( compressedDataSize <= 0 )
		{
			std::cerr << "data not compressed";
		}
		else
		{
			std::ofstream out;
			out.open (outPath, std::ios_base::binary | std::ios_base::out);
			if( out.is_open ())
			{
				out.write( compressedData, compressedDataSize );
				out.close ();
				result = true;
			}
		}
		delete [] buffer;
		delete [] compressedData;
	}
	return result;
}

bool X::CLZ4::decompressFile(std::string inPath, std::string outPath)
{
	bool result = false;
	std::ifstream file;
	file.open(inPath, std::ios_base::binary|std::ios_base::in);
	if( file.is_open())
	{
		//read all file
		file.seekg (0, std::ifstream::end);
		int size = file.tellg();
		file.clear();
		file.seekg ( 0, std::ifstream::beg );
		char* buffer = new char[size];
		file.read( buffer, size );
		file.close ();
		int maxDecompresedSize = size*255;
		char* decompressed;
		try
		{
			decompressed = new char[maxDecompresedSize];
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			delete [] buffer;
			return result;
		}
		int decompressedDataSize = LZ4_decompress_safe ( buffer, decompressed, size, maxDecompresedSize );
		if( decompressedDataSize <= 0 )
		{
			std::cerr << "data not decompressed";
		}
		else
		{
			std::ofstream out;
			out.open (outPath, std::ios_base::binary | std::ios_base::out);
			if( out.is_open ())
			{
				out.write( decompressed, decompressedDataSize );
				out.close ();
				result = true;
			}
			delete [] buffer;
			delete [] decompressed;
		}
	}
	return result;
}
