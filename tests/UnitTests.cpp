#include "../include/CLZ4.h"
#include <fstream>
#include <gtest/gtest.h>
TEST(clz4, compress) {
  std::string fileName = "test.txt";
  int n = 10;
  // create file
  std::fstream file;
  file.open(fileName, std::ios_base::out);
  EXPECT_TRUE(file.is_open()) << "initail file not open";
  for (int i = 0; i < n; ++i) {
	file << i << "\n";
  }
  file.close();
  // compress file
  std::string cFile = "test.lz4";
  std::string dFile = "test.txt";
  EXPECT_TRUE(X::CLZ4::compressFile(dFile, cFile)) << "file not compressed";
  // decompress file
  EXPECT_TRUE(X::CLZ4::decompressFile(cFile, dFile)) << "file not decopressed";
  // compare file
  std::fstream result;
  result.open(dFile, std::ios_base::in);
  EXPECT_TRUE(result.is_open()) << " result not open ";
  std::string line;
  int i = 0;
  while (std::getline(result, line)) {
	EXPECT_EQ(i, std::atoi(line.c_str()));
	++i;
  }
  EXPECT_EQ(i, n) << "incorrect line number in result";

  result.close();
}
